package com.iunigo;

import java.util.Scanner;

public class Juego {

    private Cubilete cubilete;
    private Jugador jugador;
    private Arbitro arbitro;

    public void iniciarJuego(){

        this.cubilete = new Cubilete();
        Scanner entrada = new Scanner(System.in);
        System.out.println("Nombre del jugador: ");
        String nombre = entrada.next();

        this.jugador = new Jugador(nombre, cubilete);
        this.arbitro = new Arbitro(cubilete,jugador);

        while(!arbitro.jugadorEsGanador())
            jugador.tirarDados();

        arbitro.mostrarGanador();
    }
}
