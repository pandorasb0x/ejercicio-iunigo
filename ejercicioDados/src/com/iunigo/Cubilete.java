package com.iunigo;

import java.util.Arrays;

public class Cubilete {
    private int[] dados;

    public Cubilete() {
        this.dados = new int[5];
        mezclar();
    }

    void mezclar(){
        for (int i=0; i<5 ;i++)
            dados[i]= (int) (1+ Math.random()*6);

    }

    boolean dadosSonIguales(){
        return Arrays.stream(dados).allMatch(dado -> dado == dados[1]);
    }

    public int[] getDados() {
        return dados;
    }

    public void setDados(int[] dados) {
        this.dados = dados;
    }

    @Override
    public String toString() {
        return  Arrays.toString(dados);
    }
}
