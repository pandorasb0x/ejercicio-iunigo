package com.iunigo;

public class Arbitro {

    private Cubilete cubilete;
    private Jugador jugador;

    public Arbitro(Cubilete cubilete, Jugador jugador) {
        this.cubilete = cubilete;
        this.jugador = jugador;
    }

    void mostrarGanador(){
        System.out.println("El jugador "+ jugador.getNombre() + " realiz0 " +jugador.getIntentos() +
                " intentos para conseguir el juego " + cubilete.toString());
    }

    boolean jugadorEsGanador(){
        return cubilete.dadosSonIguales();
    }


}
