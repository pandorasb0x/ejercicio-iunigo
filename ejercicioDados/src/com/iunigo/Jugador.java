package com.iunigo;

public class Jugador {

    private String nombre;
    private int intentos;
    private Cubilete cubilete;

    public Jugador(String nombre, Cubilete cubilete) {
        this.nombre = nombre;
        this.intentos = 0;
        this.cubilete = cubilete;
    }

    void tirarDados(){
        cubilete.mezclar();
        intentos++;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIntentos() {
        return intentos;
    }
}
