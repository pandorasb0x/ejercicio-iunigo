package com.iunigo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;


class JugadorTest {

    @Test
    void Intentos1() {

        Cubilete cub = new Cubilete();
        Jugador p1 = new Jugador("Cecy",cub);

        p1.tirarDados();

        Assert.assertEquals(1, p1.getIntentos());
    }

    @Test
    void Intentos4() {

        Cubilete cub = new Cubilete();
        Jugador p1 = new Jugador("Cecy",cub);

        p1.tirarDados();
        p1.tirarDados();
        p1.tirarDados();
        p1.tirarDados();

        Assert.assertEquals(4, p1.getIntentos());
    }

}