package com.iunigo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;


class ArbitroTest {

    @Test
    void esGanador() {

        int[] dados = {1,1,1,1,1};

        Cubilete cub = new Cubilete();
        cub.setDados(dados);

        Jugador p1 = new Jugador("Cecy",cub);
        Arbitro arb = new Arbitro(cub,p1);

        Assert.assertTrue(arb.jugadorEsGanador());
    }

    @Test
    void noEsGanador() {

        int[] dados = {1,1,2,1,1};

        Cubilete cub = new Cubilete();
        cub.setDados(dados);

        Jugador p1 = new Jugador("Cecy",cub);
        Arbitro arb = new Arbitro(cub,p1);

        Assert.assertFalse(arb.jugadorEsGanador());
    }
}