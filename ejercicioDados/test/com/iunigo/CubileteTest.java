package com.iunigo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;


class CubileteTest {

    @Test
    void dadosSIsonIguales() {

        int[] dados = {1,1,1,1,1};

        Cubilete cub = new Cubilete();
        cub.setDados(dados);

        Assert.assertTrue(cub.dadosSonIguales());
    }

    @Test
    void dadosNOsonIguales() {

        int[] dados = {1,2,3,4,5};

        Cubilete cub = new Cubilete();
        cub.setDados(dados);

        Assert.assertFalse(cub.dadosSonIguales());
    }


}